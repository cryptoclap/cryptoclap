# CryptoClap

## Say 'Thanks' to Anyone on GitLab by Sending a Cryptocurrency Tip!


## Overview
CryptoClap is a way to send to unsolicited Cryptocurrency tips to your fellow GitLab users -- *no need for the recipient to know anything about Cryptocurrency*.  
      
I've always appreciated how helpful GitLab contributers are and was unsatisfied with my options to say "Thanks" to a developer who, perhaps, fixed a bug that I desperately needed addressed.  

Now, with CryptoClap, just write a simple ***CryptoClap Command*** (see below) in any public GitLab Issue comment and you can send a small amount of Cryptocurrency to any GitLab user.  


## FAQ

 **- Aren't there fees for sending Bitcoin?**
 
Yes, and those [fees](https://bitinfocharts.com/comparison/bitcoin-transactionfees.html) make it impossible to use Bitcoin for CryptoClap.  Instead, CryptoClap uses a fast, feeless cryptocurrency known as [NANO](https://nano.org/).

 **- I want to send tips, but how do I get crypto *into* my CryptoClap account?**

Ok, so you made a CryptoClap account either by receiving a tip *(congrats!)* or by following the instructions in the **CryptoClap Commands** section below.  Now you want to send a tip, but your CryptoClap account is empty.... To fill it up, you'll need some NANO cryptocurrency first.  Here are some popular services to buy NANO:

 - [AnchorUSD](https://www.anchorusd.com/)
 - [Kraken](https://www.kraken.com/)
 - [Chainbits](https://buy.chainbits.com/)
 - [Binance](https://www.binance.us/en/home)

Now, just transfer some NANO from one of those services *(e.g. [AnchorUSD](https://www.anchorusd.com/))* into your CryptoClap wallet... 
How to identify your CryptoClap wallet address?  You can either use your CryptoClap account's QR code or your CryptoClap account's public wallet address *(e.g. nano_234j2l3kj4234243)*.  Find both of those in your CryptoClap activity log [here](%5Bhere%5D%28https://gitlab.com/cryptoclap/cryptoclap/-/issues%29).


 **- Should I store a lot of NANO in my CryptoClap account?**

 No.  If you receive a CryptoClap tip from another user, go ahead and transfer that NANO to whichever reputable [NANO wallet](https://natrium.io/) you choose. See the ***CryptoClap Commands*** section for instructions for transferring your NANO off CryptoClap.  Here are two popular NANO wallets unaffiliated with CryptoClap:
 
 - [NANO Vault](https://nanovault.io/)
 - [Natrium](https://natrium.io/)


 **-Is my NANO safe in my CryptoClap wallet?**

Behave as if it's NOT.  Most user *don't* put a lot of NANO in their CryptoClap accounts --- just enough for a sending a few tips here and there.  Similar to how most people leave savings in a bank account and only carry a small amount of cash in their pocket, you might view CryptoClap the same way.     


 **-I tipped a GitLab user with CryptoClap and nothing happened**

 Verify that you posted a valid ***CryptoClap Command*** (see below) in a GitLab Comment or Issue Description on a **public** repository.  If so, check your available funds by sending a CryptoClap balance command.


 **-Isn't sending tips contrary to the spirit of the Open Source community?**

 [No](https://opensource.com/education/12/7/clearing-open-source-misconceptions).
 

 **-I accidentally tipped more than I meant to or I tipped the wrong GitLab user...Can you reverse the transaction?**

 No.  Your ***CryptoClap Command*** published a transaction to the global NANO [blockchain](https://nanocrawler.cc/) and is irreversible.  You are in control of the crypto sitting in your CryptoClap account.  So, reminder #37 to not keep a lot of crypto in your CryptoClap account.

## CryptoClap Commands
Simply type a CryptoClap command in any public Gitlab Issue Description or Comment!
All command responses will be logged [here](https://gitlab.com/cryptoclap/cryptoclap/-/issues)

 - **Create a CryptoClap account (automatic for tip recipients)**

		@CryptoClap create account

 - **Send crypto to a GitLab user**

		@CryptoClap send 0.35 NANO to @SomeGitlabUser
		
 - **Transfer your crypto off of CryptoClap**
 
  		@CryptoClap send 3.14 NANO to [insert your NANO wallet address e.g. nano_3n3f82323nef.....]

 - **Check your CryptoClap balance**

		@CryptoClap balance



## Misc
Copyright p0rtals Technology Group, 2021

